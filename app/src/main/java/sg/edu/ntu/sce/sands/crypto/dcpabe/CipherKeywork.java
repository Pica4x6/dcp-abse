package sg.edu.ntu.sce.sands.crypto.dcpabe;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sg.edu.ntu.sce.sands.crypto.dcpabe.ac.AccessStructure;


public class CipherKeywork implements Serializable {
	private static final long serialVersionUID = 1L;
	private byte[] ckw1;
	private byte[] ckw2;
	private byte[] ckw3;
	private List<byte[]> Wx;
	private List<byte[]> Wprime;
	private AccessStructure accessStructure;

	public CipherKeywork() {
		Wx = new ArrayList<byte[]>();
		Wprime = new ArrayList<byte[]>();
	}

	public byte[] getCkw1() {
		return ckw1;
	}

	public void setCkw1(byte[] c0) {
		this.ckw1 = c0;
	}

	public byte[] getCkw2() {
		return ckw2;
	}

	public void setCkw2(byte[] c0) {
		this.ckw2 = c0;
	}

	public byte[] getCkw3() {
		return ckw3;
	}

	public void setCkw3(byte[] c0) {
		this.ckw3 = c0;
	}

	public byte[]  getWx(int x) {
		return Wx.get(x);
	}

	public void setWx(byte[] c1x) {
		Wx.add(c1x);
	}

	public byte[]  getWprime(int x) {
		return Wprime.get(x);
	}

	public void setWprime(byte[] c1x) {
		Wprime.add(c1x);
	}

	public void setAccessStructure(AccessStructure accessStructure) {
		this.accessStructure = accessStructure;
	}
	
	public AccessStructure getAccessStructure() {
		return accessStructure;
	}
}
