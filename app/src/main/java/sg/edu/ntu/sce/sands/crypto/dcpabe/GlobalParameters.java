package sg.edu.ntu.sce.sands.crypto.dcpabe;
import java.io.IOException;
import java.io.Serializable;

import it.unisa.dia.gas.jpbc.CurveParameters;
import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

public class GlobalParameters implements Serializable {
	private static final long serialVersionUID = 1L;
	private CurveParameters curveParams;
	private Element g1;
	private Element g1R;
	private Element g1S;
	private Element g1T;
	private Element R;
	private Element S;
	private Element T;
	public CurveParameters getCurveParams() {
		return curveParams;
	}
	public void setCurveParams(CurveParameters curveParams) {
		this.curveParams = curveParams;
	}
	public Element getG1() {
		return g1;
	}
	public Element getR() {
		return R;
	}
	public Element getS() {
		return S;
	}
	public Element getT() {
		return T;
	}
	public Element getg1R() {
		return g1R;
	}
	public Element getg1S() {
		return g1S;
	}
	public Element getg1T() {
		return g1T;
	}
	public void setG1(Element g1) {
		this.g1 = g1;
	}
	public void setR(Element ri) {
		this.R = ri;
		this.g1R = this.g1.powZn(ri);
	}
	public void setS(Element si) {
		this.S = si;
		this.g1S = this.g1.powZn(si);
	}
	public void setT(Element ti) {
		this.T = ti;
		this.g1T = this.g1.powZn(ti);
	}


	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeObject(curveParams);
		out.writeObject(g1.toBytes());
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		curveParams = (CurveParameters) in.readObject();
		Pairing pairing = PairingFactory.getPairing(curveParams);
		g1 = pairing.getG1().newElement();
		g1.setFromBytes((byte[]) in.readObject());
		g1 = g1.getImmutable();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (null != obj)
			if (obj instanceof GlobalParameters) {
				GlobalParameters other = (GlobalParameters) obj;
				if (curveParams.equals(other.curveParams))
					if (g1.isEqual(other.g1))
						return true;
			}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(curveParams.toString());
		sb.append(g1.toString());
		sb.append(R.toString());
		sb.append(S.toString());
		sb.append(T.toString());

		return sb.toString();
	}
}
