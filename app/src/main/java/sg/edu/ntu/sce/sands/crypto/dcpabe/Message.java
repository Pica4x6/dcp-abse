package sg.edu.ntu.sce.sands.crypto.dcpabe;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;

public class Message {
	public byte[] m;
	public String content;
	
	public Message() {
		
	}
	
	public Message(byte[] m) {
		this.m = m;
	}

	public Message(String f, AssetManager assetManager){
		InputStream input;

		try {
			input = assetManager.open(f);
			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			this.m = buffer;
			this.content = new String(buffer);

		}
		catch (IOException e){
			this.m = new byte[0];
		}
	}
}