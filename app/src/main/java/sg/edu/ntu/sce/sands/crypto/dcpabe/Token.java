package sg.edu.ntu.sce.sands.crypto.dcpabe;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sg.edu.ntu.sce.sands.crypto.dcpabe.ac.AccessStructure;


public class Token implements Serializable {
    private static final long serialVersionUID = 1L;
    private byte[] tok1;
    private byte[] tok2;
    private byte[] tok3;
    private List<byte[]> Vx;
    private List<byte[]> Vprime;

    private AccessStructure accessStructure;

    public Token() {
        Vx = new ArrayList<byte[]>();
        Vprime = new ArrayList<byte[]>();
    }

    public byte[] getTok1() {
        return tok1;
    }

    public void setTok1(byte[] tk) {
        this.tok1 = tk;
    }

    public byte[] getTok2() {
        return tok2;
    }

    public void setTok2(byte[] tk) {
        this.tok2 = tk;
    }

    public byte[] getTok3() {
        return tok3;
    }

    public void setTok3(byte[] tk) {
        this.tok3 = tk;
    }

    public byte[]  getVx(int x) {
        return Vx.get(x);
    }

    public void setVx(byte[] c1x) {
        Vx.add(c1x);
    }

    public byte[] getVprime(int x) {
        return Vprime.get(x);
    }

    public void setVprime(byte[] c2x) {
        Vprime.add(c2x);
    }


    public void setAccessStructure(AccessStructure accessStructure) {
        this.accessStructure = accessStructure;
    }

    public AccessStructure getAccessStructure() {
        return accessStructure;
    }
}
