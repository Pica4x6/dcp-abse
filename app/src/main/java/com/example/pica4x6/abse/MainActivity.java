package com.example.pica4x6.abse;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.app.Activity;
import android.view.View;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import it.unisa.dia.gas.jpbc.Element;
import it.unisa.dia.gas.jpbc.Pairing;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;
import sg.edu.ntu.sce.sands.crypto.dcpabe.AuthorityKeys;
import sg.edu.ntu.sce.sands.crypto.dcpabe.CipherKeywork;
import sg.edu.ntu.sce.sands.crypto.dcpabe.Ciphertext;
import sg.edu.ntu.sce.sands.crypto.dcpabe.DCPABE;
import sg.edu.ntu.sce.sands.crypto.dcpabe.GlobalParameters;
import sg.edu.ntu.sce.sands.crypto.dcpabe.Message;
import sg.edu.ntu.sce.sands.crypto.dcpabe.PersonalKeys;
import sg.edu.ntu.sce.sands.crypto.dcpabe.PublicKeys;
import sg.edu.ntu.sce.sands.crypto.dcpabe.Token;
import sg.edu.ntu.sce.sands.crypto.dcpabe.ac.AccessStructure;


public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "DCP-ABSE";
    protected boolean running = false;
    protected int ite = 1;
    protected int nbits = 160;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonLaunch).setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view) {
        Log.i(TAG, "onClick");


        //findViewById(R.id.progress).setVisibility(View.VISIBLE);
        //((TextView) findViewById(R.id.status)).setText("Benchmarking...");
        //((Button) findViewById(R.id.buttonLaunch)).setText("Stop");
        ite = Integer.parseInt(((EditText) findViewById(R.id.iteNumber)).getText().toString());
        nbits = Integer.parseInt(((EditText) findViewById(R.id.bitsNumber)).getText().toString());
        Log.i(TAG, String.valueOf(ite));
        running = true;
        findViewById(R.id.buttonLaunch).setVisibility(View.INVISIBLE);
        new Thread(new Runnable(){
            public void run() {
                ppal();
            }
        }).start();

        running = false;

        Log.i(TAG, "onClick.finished");
    }

    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(android.os.Message message){
            switch (message.what){
                case 1: ((TextView) findViewById(R.id.globalSetupText)).setText(message.obj.toString());
                    break;
                case 2: ((TextView) findViewById(R.id.authorityText)).setText(message.obj.toString());
                    break;
                case 3: ((TextView) findViewById(R.id.keyGenText)).setText(message.obj.toString());
                    break;
                case 4: ((TextView) findViewById(R.id.dataEncryptionText)).setText(message.obj.toString());
                    break;
                case 5: ((TextView) findViewById(R.id.decryptionText)).setText(message.obj.toString());
                    break;
                case 6: ((TextView) findViewById(R.id.textgr)).setText(message.obj.toString());
                    break;
                case 7: ((TextView) findViewById(R.id.textegg)).setText(message.obj.toString());
                    break;
                case 8: ((TextView) findViewById(R.id.textgrgr)).setText(message.obj.toString());
                    break;
                case 9: ((TextView) findViewById(R.id.texth1)).setText(message.obj.toString());
                    break;
                case 10: ((TextView) findViewById(R.id.keyWordEncryptionText)).setText(message.obj.toString());
                    break;
                case 11: ((TextView) findViewById(R.id.tokenText)).setText(message.obj.toString());
                    break;
                case 12: ((TextView) findViewById(R.id.searchText)).setText(message.obj.toString());
                    break;
                case 13: ((TextView) findViewById(R.id.textCtSize)).setText(message.obj.toString());
                    break;
                case 14: ((TextView) findViewById(R.id.textCkwSize)).setText(message.obj.toString());
                    break;
                case 15: findViewById(R.id.buttonLaunch).setVisibility(View.VISIBLE);
                    break;
            }

        }
    };

    protected void ppal() {
        long lStartTime;
        long lEndTime;
        double difference;
        double aux = 0;
        GlobalParameters gp = null;
        AuthorityKeys authority1 = null;
        PersonalKeys pkeys = null;
        Ciphertext ct = null;
        CipherKeywork ckw = null;
        Token tk = null;
        Message dmessage = null;


        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            gp = DCPABE.globalSetup(nbits);
            Log.e(TAG,gp.getCurveParams().toString());
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }
        }
        android.os.Message message = new android.os.Message();
        message.obj = aux;
        message.what = 1;
        mHandler.sendMessage(message);

        PublicKeys publicKeys = new PublicKeys();
        DCPABE.masterKeyGen(gp);

        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            authority1 = DCPABE.authoritySetup("a1", gp, "a", "b", "c", "d");
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 2;
            mHandler.sendMessage(message);

            publicKeys.subscribeAuthority(authority1.getPublicKeys());

        }


        for (int i=0; i<ite; i++) {
            pkeys = new PersonalKeys("user");
            lStartTime = new Date().getTime();
            pkeys.addKey(DCPABE.keyGen("user", "a", authority1.getSecretKeys().get("a"), gp));
            lEndTime = new Date().getTime();
            pkeys.addKey(DCPABE.keyGen("user", "b", authority1.getSecretKeys().get("b"), gp));
            pkeys.addKey(DCPABE.keyGen("user", "c", authority1.getSecretKeys().get("c"), gp));
            pkeys.addKey(DCPABE.keyGen("user", "d", authority1.getSecretKeys().get("d"), gp));
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 3;
            mHandler.sendMessage(message);
        }

        AccessStructure as = AccessStructure.buildFromPolicy("and a and b c");

        AssetManager assetManager = getAssets();
        Message cmessage = null;

        cmessage = new Message("heart.xml",assetManager);
        Log.e(TAG, "Archivo.m: " + cmessage.m + "\n" );
        Log.e(TAG, "Archivo.m.length: " + cmessage.m.length + "\n" );
        //Log.e(TAG, "Archivo.string: " + cmessage.toString() + "\n" );
        //Log.e(TAG, "Archivo.content: " + cmessage.content + "\n" );


        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            ct = DCPABE.encrypt(cmessage, as, gp, publicKeys);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 4;
            mHandler.sendMessage(message);

            message = new android.os.Message();
            message.obj = cmessage.m.length;
            message.what = 13;
            mHandler.sendMessage(message);
        }

        Log.e(TAG, "ct.toString(): " + ct.toString() + "\n" );

        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            ckw = DCPABE.kwEncrypt(cmessage, as, gp, publicKeys);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 10;
            mHandler.sendMessage(message);

            message = new android.os.Message();
            message.obj = cmessage.m.length;
            message.what = 14;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            tk = DCPABE.tokenGen(cmessage, as, gp, pkeys);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 11;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            dmessage = DCPABE.decrypt(ct, pkeys, gp);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 5;
            mHandler.sendMessage(message);
        }

        Log.e(TAG, "dmessage.toString(): " + dmessage.toString() + "\n" );
        Log.e(TAG, "dmessage.content: " + dmessage.content + "\n" );
        Log.e(TAG, "dmessage.m: " + dmessage.m + "\n" );
        Log.e(TAG, "dmessage.m.length: " + dmessage.m.length + "\n" );

        for (int i=0; i<ite; i++) {
            lStartTime = new Date().getTime();
            Boolean search = DCPABE.search(ckw, pkeys, gp, tk);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 12;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            Pairing pairing = PairingFactory.getPairing(gp.getCurveParams());
            Element yi = pairing.getZr().newRandomElement().getImmutable();
            lStartTime = new Date().getTime();
            byte [] exponente = gp.getG1().powZn(yi).toBytes();
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 6;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            Pairing pairing = PairingFactory.getPairing(gp.getCurveParams());
            lStartTime = new Date().getTime();
            Element eg1g1 = pairing.pairing(gp.getG1(), gp.getG1()).getImmutable();
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 7;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            Pairing pairing = PairingFactory.getPairing(gp.getCurveParams());
            Element yi = pairing.getZr().newRandomElement().getImmutable();
            Element ai = pairing.getZr().newRandomElement().getImmutable();
            byte [] exponente = gp.getG1().powZn(yi).toBytes();
            byte [] exponente2 = gp.getG1().powZn(ai).toBytes();
            lStartTime = new Date().getTime();
            byte [] exponente3 = gp.getG1().powZn(yi).mul(gp.getG1().powZn(ai)).toBytes();
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 8;
            mHandler.sendMessage(message);
        }

        for (int i=0; i<ite; i++) {
            Pairing pairing = PairingFactory.getPairing(gp.getCurveParams());
            Element yi = pairing.getZr().newRandomElement().getImmutable();
            lStartTime = new Date().getTime();
            Element HGID = pairing.getG1().newElement();
            HGID.setFromHash("a".getBytes(), 0, "a".getBytes().length);
            lEndTime = new Date().getTime();
            difference = (lEndTime - lStartTime) / 1000.0;

            if (i==0) {
                aux = difference;
            } else {
                aux = (aux + difference) / 2;
            }

            message = new android.os.Message();
            message.obj = aux;
            message.what = 9;
            mHandler.sendMessage(message);
        }

        message = new android.os.Message();
        message.obj = "";
        message.what = 15;
        mHandler.sendMessage(message);


    }
}
